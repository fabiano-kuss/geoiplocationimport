# Import and Use Georeferenced database

A python and nodejs apps to import and use Georeferenced database using Mongo database and web service.


This project transform the csv in document and make the  octet IP address in a long number by the way simplify and optimize the search.

## Get database files

Import the GeoLite2 City and GeoLite2 ASN zip files from https://dev.maxmind.com/geoip/geoip2/geolite2/

This step should to used only once.

Extract the files in the same directory where import.py file is.


## Configure and load import process

Change the files names and database settings into import.py whether it is necessary.

Load the importation app with 

```shell
python import.py
```

This step spend some time, waiting while the application import informations, create documents and insert it into database.

## Configure and load web service

After you have npm and node instaled use the follow commands

```shell
npn install
node find_location.js
```

Open your browser and type the url http://localhost:8081/getlocation?ip=161.148.0.12 to test the application
