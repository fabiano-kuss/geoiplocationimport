import json, sys
from pymongo import MongoClient

#Open source data files
country_file = open("GeoLite2-City-Locations-pt-BR.csv")
ips_file =  open("GeoLite2-City-Blocks-IPv4.csv")
ips_asn =  open("GeoLite2-ASN-Blocks-IPv4.csv")

headers_city = [{'geoname_id':0},{'locale_code':1},{'continent_code':1},{'continent_name':1},{'country_iso_code':1},{'country_name':1},{'subdivision_1_iso_code':1},{'subdivision_1_name':1},{'subdivision_2_iso_code':1},{'subdivision_2_name':1},{'city_name':1},{'metro_code':1},{'time_zone':1},{'is_in_european_union':0}]

headers_ip = [{'network':1},{'geoname_id':0},{'registered_country_geoname_id':0},{'represented_country_geoname_id':0},{'is_anonymous_proxy':0},{'is_satellite_provider':0},{'postal_code':1},{'latitude':0},{'longitude':0},{'accuracy_radius':0}]

headers_asn = [{'network':1},{'autonomous_system_number':1},{'autonomous_system_organization':1}]

valid_city_fields = [0, 2, 3, 4, 5, 6, 8, 10]
valid_ip_fields = range(0, len(headers_ip))
valid_asn_fields = [1,2]

#READ HEADER
line = country_file.readline()
line = country_file.readline()

cities = {}
asn = {}
asn_ips = []

def get_asn(value):

    json_data = ""


    for i in range(len(headers_asn), len(value)):
        value[len(value) - 1] += ", "+value[len(value) - 1].replace("\"", "").replace("'", "").replace("\\", "/")

    for i in valid_asn_fields:
        if headers_asn[i].items()[0][1] == 1:
            json_data  += '"%s":"%s",' %(headers_asn[i].items()[0][0], value[i].replace('"', '').replace("\\", "/"))
        else:
            insert_value = value[i].strip()
            insert_value =  -1 if len(insert_value) < 1 else insert_value
            json_data  += '"%s":%s,' %(headers_asn[i].items()[0][0], insert_value)


    json_data = json.loads("{%s}" %(json_data[0:-1]))

    return json_data




def get_ips(value):
    json_data = ""

    for i in valid_ip_fields:
        if headers_ip[i].items()[0][1] == 1:
            json_data  += '"%s":"%s",' %(headers_ip[i].items()[0][0], value[i].replace('"', ''))
        else:
            insert_value = value[i].strip()
            insert_value =  -1 if len(insert_value) < 1 else insert_value
            json_data  += '"%s":%s,' %(headers_ip[i].items()[0][0], insert_value)
    json_data = json.loads("{%s}" %(json_data[0:-1]))

    return json_data


def get_city(value):

    json_data = ""
    for i in valid_city_fields:
        if headers_city[i].items()[0][1] == 1:
            json_data  += '"%s":"%s",' %(headers_city[i].items()[0][0], value[i].replace('"', ''))
        else:
            json_data  += '"%s":%s,' %(headers_city[i].items()[0][0], value[i].replace('"', ''))

    json_data = json.loads("{%s}" %(json_data[0:-1]))
    return json_data

def ip_calc(ip_num, cidr):
    ip = ip_num.split(".")
    if len(ip) < 4:
        print "IP LEN ERROR"
        return 


    init = 0
    for i in ip:
        init <<= 8
        init |= int(i)


    mask = 0
    for i in range(0, 32 - cidr):
        mask <<= 1
        mask |= 1


    finish = init + ((init & 0xFF)^mask)

    return [init, finish]
    ''' Return the long ip value to string ip format
    str_res = ""
    for i in range(0, 4):
        str_res = str(result & 0xFF) +"."+str_res
        result >>= 8
        print str_res[:-1]
    '''

print "GETING LOCATION INFORMATIONS"
while line:
    data = line.split(",")
    cities[data[0]] = get_city(data)
    line = country_file.readline()

print "Cities:", len(cities)

#READ ASN HEADER
line = ips_asn.readline()
line = ips_asn.readline()

print "GETING ASN INFORMATIONS"
while line:
    data = line.replace("\n", "").split(",")
    asn[data[0]] = get_asn(data)
    iprange = data[0].split("/")
    values = ip_calc(iprange[0], int(iprange[1]))
    asn_ips.append([values[0], values[1], data[0]])
    line = ips_asn.readline()

print "ASN", len(asn)

#READ LOCATION HEADER
line = ips_file.readline()
line = ips_file.readline()

client = MongoClient('localhost', 27017)
db = client["statistics"]
coll = db["ip_range"]
print "INSERTING DOCUMENTS ON DATABASE\nREGISTERS: "

def getASNByInterval(init, finish):
    pini = 0
    pfinish = len(asn_ips)
    pactual = pfinish / 2
    while True:
        if asn_ips[pactual][0] <= init:
            if asn_ips[pactual][1] >= finish:
                return asn[asn_ips[pactual][2]]
                break
            if pfinish - pactual < 2:
                return None
            pini = pactual
            pactual =  (pini + pfinish) / 2
        elif asn_ips[pactual][0] >= init:
            if pactual - pini < 2:
                return None
            pfinish = pactual
            pactual =  (pini + pfinish) / 2
        else:
            return asn[asn_ips[pactual][2]]


#ip_range = ip_calc("161.148.0.1", 16)
#print "GET THIS IP", getASNByInterval(ip_range[0], ip_range[1])

i=0
while line:
    data = line.split(",")
    value = get_ips(data)
    if data[1].strip() == '':
        data[1] = data[2]
    try:
        value['location'] = cities[data[1]]
        split_ip = value['network'].split("/")
        ip_range = ip_calc(split_ip[0], int(split_ip[1]))
        if asn.has_key(value['network']):
            value['autonomous_system_organization'] =  asn[value['network']]
        else:
            asnValue = getASNByInterval(ip_range[0], ip_range[1])
            if asnValue != None:
                value['autonomous_system_organization'] = asnValue
        value['init_ip'] = ip_range[0]
        value['end_ip'] = ip_range[1]
    except:
        print "NO CRITICAL ERROR: ", data
  
    coll.insert(value)
    i+=1
    if i % 1000 == 0:
        sys.stdout.write('.')
        sys.stdout.flush()
    line = ips_file.readline()

print "DONE"
