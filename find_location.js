var mongo = require('mongodb').MongoClient, 
   express = require('express'),
   bodyParser = require('body-parser');

//TODO - Change it to production installation
var url = 'mongodb://localhost';
var app = express();
var IPPORT=8081;

app.get("/getlocation", function(req, res){

	splited = req.query.ip.split(".");

	console.log(splited);

	try{
 		mongo(url,  { useNewUrlParser: true }).connect(function(err, database){
         		var coll = database.db('statistics').collection('ip_range');
			//Transform the 4 octets address into a long 
			startIp = splited.reduce(function(ipInt, octet) { return (ipInt<<8) + parseInt(octet, 10)}, 0) >>> 0
			console.log(startIp);
			//Find by interval
         		coll.findOne({$and:[{init_ip:{$lte:startIp}}, {end_ip:{$gte:startIp}}]}, function(err, result) {
            			res.setHeader('Content-Type', 'application/json');
				console.log(result);
         			res.send(result);
         		});
      		});
   	}catch(err){
		console.log(err);
		res.send(err);
   	}
});

app.listen(IPPORT);
console.log('Listening on port '+IPPORT);
